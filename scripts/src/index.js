/*
 BSC TEST NET
 Bridge Oracle Address: 0xd4ae595db0A3075b05bEBb36260FD2120585ae24
 Bridge Address: 0x35932590A693255B7262A45e59F363CE3654df89
 Test Token: 0x75d4225Cc97Cb978b4F680aB8135d6555b12e582

*/

const fs = require('fs');
const Web3 = require('web3');
const abi = require('../assets/abi.json');
const data = require('../assets/data.json');

const web3Bsc = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545');

const adminPrivKey = '[adminPrivKey]';
const { address: admin } = web3Bsc.eth.accounts.wallet.add(adminPrivKey);

const bridge = new web3Bsc.eth.Contract(
  abi,
  data.bsc.contract
);

(async () => {
  const bscCurrentBlock = await web3Bsc.eth.getBlockNumber();
  const bscFromBlock = ++data.bsc.lastChecked;
  let bscToBlock = bscCurrentBlock - 5;

  if(4999 < bscToBlock - bscFromBlock) {
    bscToBlock = bscFromBlock + 4999;
  }

  const options = {
      filter: {step: [0]},
      fromBlock: bscFromBlock,
      toBlock: bscToBlock
  };

  bridge.getPastEvents(
    'Transfer',
    options,
    (err, events) => {
      if(err) {
        console.log(err);
        return;
      }

      events.forEach(async (event) => {
        const { from, to, amount, fee, date, nonce, step } = event.returnValues;

        console.log(`
          Processed transfer:
          - from ${from} 
          - to ${to} 
          - amount ${amount} tokens
          - date ${date}
          - step ${step}
        `);

        try {
          const tx = bridge.methods.payout(to, amount, fee, nonce);
          const [gasPrice, gasCost] = await Promise.all([
            web3Bsc.eth.getGasPrice(),
            tx.estimateGas({from: admin}),
          ]);

          const transaction = {
            from: admin,
            to: bridge.options.address,
            data: tx.encodeABI(),
            gas: gasCost,
            gasPrice
          };
        
          const receipt = await web3Bsc.eth.sendTransaction(transaction);
          console.log(`Transaction hash: ${receipt.transactionHash}`);
        } catch (e) {
          console.log(e)
        }
      });
    }
  );

  data.bsc.lastChecked = bscToBlock;
  fs.writeFile('assets/data.json', JSON.stringify(data), err => {
    if (err) {
      throw err
    }
  });

})();

/*
const dataString = JSON.stringify(data)
fs.writeFile('assets/data.json', dataString, err => {
  if (err) {
    throw err
  }
  console.log('JSON data is saved.')
});
*/

/*
test.events.Transfer(options)
  .on('data', event => console.log(event))
  .on('changed', changed => console.log(changed))
  .on('error', err => console.log(err))
  .on('connected', str => console.log(str));
*/

/*
test.events.Transfer(
  {fromBlock: 0, step: 0}
)
.on('data', async event => {
  const { from, to, amount, date, nonce } = event.returnValues;
  
  console.log(`
    Processed transfer:
    - from ${from} 
    - to ${to} 
    - amount ${amount} tokens
    - date ${date}
  `);
});

setTimeout(function(){
  console.log("test");
}, 50000);
*/