// SPDX-License-Identifier: WTFPL
pragma solidity ^0.8.0;
import "@openzeppelin/contracts@4.6.0/utils/Counters.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import './TXToken.sol';
contract BridgeBsc is Ownable {
    using Counters for Counters.Counter;
    
    address public oracle;
    TXToken public token;
    
    uint public fee;
    uint256 public minFee;
    address public feeCollector;
    
    Counters.Counter private nonce;
    
    mapping(uint => bool) public processedNonces;

    modifier onlyOracle() {
        require(
            msg.sender == oracle,
            "only oracle"
        );
        
        _;
    }

    enum Step { Bridge, Payout }

    event Transfer(
        address from,
        address to,
        uint256 amount,
        uint256 fee,
        uint date,
        uint256 nonce,
        Step indexed step
    );

    constructor() {
        token = new TXToken();
        minFee = 40000000000;
        fee = 1000;
    }

    function updateOracle(address newOracle)
    external onlyOwner
    {
        oracle = newOracle;
    }

    function updateFeeCollector(address newFeeCollector)
    external onlyOwner
    {
        feeCollector = newFeeCollector;
    }

    function updateMinFee(uint256 newMinFee)
    external onlyOwner
    {
        minFee = newMinFee;
    }

    function updateFee(uint newFee)
    external onlyOwner
    {
        fee = newFee;
    }

    function bridge(address to, uint256 amount) 
    external
    {
        require(
            amount > minFee,
            "Amount to low"
        );

        uint256 feeAmount = SafeMath.div(amount, fee);
        if (feeAmount < minFee) {
            feeAmount = minFee;
        }
        
        token.burnFrom(
            msg.sender,
            SafeMath.sub(amount, feeAmount)
        );

        token.transferFrom(
            msg.sender,
            feeCollector,
            feeAmount
        );

        emit Transfer(
            msg.sender,
            to,
            SafeMath.sub(amount, feeAmount),
            feeAmount,
            block.timestamp,
            nonce.current(),
            Step.Bridge
        );

        nonce.increment();
    }

    function payout(address to, uint256 amount, uint256 otherChainFee, uint256 otherChainNonce)
    external onlyOracle
    {
        require(processedNonces[otherChainNonce] == false, 'transfer already processed');
        processedNonces[otherChainNonce] = true;
        token.mint(to, amount);
        emit Transfer(
            msg.sender,
            to,
            amount,
            otherChainFee,
            block.timestamp,
            otherChainNonce,
            Step.Payout
        );
    }
}